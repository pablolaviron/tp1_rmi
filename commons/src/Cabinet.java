import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Cabinet extends Remote {
    boolean addAnimal(String name, String ownerName, String speciesName, int speciesAverageLife, String race, String state) throws RemoteException;
    boolean addAnimal(String name, String ownerName, Species species, String race, String state) throws RemoteException;
    ArrayList<Animal> getAnimals() throws RemoteException;
    Animal getAnimalByName(String name) throws RemoteException;
    void registerObserver(CabinetObserver observer) throws RemoteException;
    void unregisterObserver(CabinetObserver observer) throws RemoteException;
}
