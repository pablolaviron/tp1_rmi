import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FollowUpFile extends Remote {
    String read() throws RemoteException;
    void add(String state) throws RemoteException;
}
