import java.io.Serializable;

public class Species implements Serializable {

    String name;
    int averageLife;

    public Species(String name, int averageLife) {
        this.name = name;
        this.averageLife = averageLife;
    }

    public String getName() {
        return name;
    }

    public int getAverageLife() {
        return averageLife;
    }
}
