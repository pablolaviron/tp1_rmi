import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CabinetObserver extends Remote {
    void onPatientCountEqualsThreshold(int threshold) throws RemoteException;
}
