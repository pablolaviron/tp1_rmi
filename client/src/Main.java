import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Main {
    public static void main(String[] args) {
        try {
            Client client = new Client();
            client.setUp();
            Cabinet cabinet = client.lookupCabinet("Animal Care");
            CabinetObserver cabinetObserver = new CabinetObserverImpl(cabinet);
            System.out.println("Objet proxy de l'objet cabinet : " + cabinet);
            client.browseCabinet();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}
