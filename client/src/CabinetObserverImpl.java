import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CabinetObserverImpl extends UnicastRemoteObject implements CabinetObserver {

    private Cabinet cabinet;

    public CabinetObserverImpl(Cabinet cabinet) throws RemoteException {
        this.cabinet = cabinet;
        cabinet.registerObserver(this);
    }

    @Override
    public void onPatientCountEqualsThreshold(int threshold) throws RemoteException {
        System.out.println("Number of patients has reached " + threshold);
    }
}
