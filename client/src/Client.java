import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Client {

    Registry registry;
    Cabinet cabinet;

    public Client() {

    }

    public void setUp() throws RemoteException {
        registry = LocateRegistry.getRegistry();
    }

    public void setUp(int port) throws RemoteException {
        registry = LocateRegistry.getRegistry(port);
    }

    public Cabinet lookupCabinet(String cabinetName) throws NotBoundException, RemoteException {
        cabinet = (Cabinet) registry.lookup(cabinetName);
        return cabinet;
    }

    public void browseCabinet() throws RemoteException {
        Scanner scanner = new Scanner(System.in);
        int action = -1;
        System.out.println("----- Welcome to the cabinet -----");
        while(action != 0) {
            System.out.print("Please select an action...\n[0] Exit\n[1] List of patients\n[2] Patient informations\n[3] Add a consultation\n[4] Add a patient\n> ");
            action = scanner.nextInt();
            if(action == 1) {
                System.out.println("List of patients :");
                for(Animal patient: cabinet.getAnimals()) {
                    System.out.println("- " + patient.getName());
                }
            }
            if(action == 2) {
                System.out.print("Please enter the animal name...\n> ");
                String animalName = scanner.next();
                Animal patient = cabinet.getAnimalByName(animalName);
                if(patient != null) {
                    System.out.println("Name : " + patient.getName());
                    System.out.println("Owner : " + patient.getOwnerName());
                    System.out.println("Species : " + patient.getSpecies().getName() + ", average life of " + patient.getSpecies().getAverageLife() + " years");
                    System.out.println("Race : " + patient.getRace());
                    System.out.println("Follow up file : \n" + patient.getFollowUpFile().read());
                } else {
                    System.out.println("No patient found for this name");
                }
            }
            if(action == 3) {
                System.out.print("Please enter the animal name...\n> ");
                String animalName = scanner.next();
                Animal patient = cabinet.getAnimalByName(animalName);
                if(patient != null) {
                    System.out.print("Please enter the state...\n> ");
                    String state = scanner.next();
                    patient.getFollowUpFile().add(state);
                } else {
                    System.out.println("No patient found for this name");
                }
            }
            if(action == 4) {
                System.out.print("Please enter the animal name...\n> ");
                String animalName = scanner.next();
                System.out.print("Please enter the owner name...\n> ");
                String ownerName = scanner.next();
                System.out.print("Please enter the species name...\n> ");
                String speciesName = scanner.next();
                if(speciesName.equals("Dog")) {
                    Dog species = new Dog();
                    System.out.print("Please enter the race...\n> ");
                    String race = scanner.next();
                    System.out.print("Please enter the state...\n> ");
                    String state = scanner.next();
                    if(cabinet.addAnimal(animalName, ownerName, species, race, state)) {
                        System.out.println("The patient has been added");
                    } else {
                        System.out.println("Can't add this patient");
                    }
                } else if(speciesName.equals("Cat")) {
                    Cat species = new Cat();
                    System.out.print("Please enter the race...\n> ");
                    String race = scanner.next();
                    System.out.print("Please enter the state...\n> ");
                    String state = scanner.next();
                    if(cabinet.addAnimal(animalName, ownerName, species, race, state)) {
                        System.out.println("The patient has been added");
                    } else {
                        System.out.println("Can't add this patient");
                    }
                } else {
                    System.out.print("Please enter the species average life...\n> ");
                    int speciesAverageLife = scanner.nextInt();
                    Species species = new Species(speciesName, speciesAverageLife);
                    System.out.print("Please enter the race...\n> ");
                    String race = scanner.next();
                    System.out.print("Please enter the state...\n> ");
                    String state = scanner.next();
                    if(cabinet.addAnimal(animalName, ownerName, species, race, state)) {
                        System.out.println("The patient has been added");
                    } else {
                        System.out.println("Can't add this patient");
                    }
                }
            }
        }
        scanner.close();
        System.exit(1);
    }

    public Cabinet getCabinet() {
        return cabinet;
    }
}
