import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;

public class Main {
    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.setUp();
        } catch(RemoteException | AlreadyBoundException e) {
            System.err.println("Server exception");
            e.printStackTrace();
        }
    }
}
