import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class CabinetImpl extends UnicastRemoteObject implements Cabinet {

    private ArrayList<Animal> patients;
    private ArrayList<CabinetObserver> observers = new ArrayList<>();
    private static final int THRESHOLD_1 = 100;
    private static final int THRESHOLD_2 = 500;
    private static final int THRESHOLD_3 = 1000;

    protected CabinetImpl() throws RemoteException {
        patients = new ArrayList<>();
    }

    @Override
    public boolean addAnimal(String name, String ownerName, String speciesName, int speciesAverageLife, String race, String state) throws RemoteException {
        Animal patient = new AnimalImpl(name, ownerName, speciesName, speciesAverageLife, race, state);
        boolean added = patients.add(patient);
        if(added && (patients.size() == THRESHOLD_1 || patients.size() == THRESHOLD_2 || patients.size() == THRESHOLD_3)) {
            notifyObserversOnPatientCountEqualsThreshold(patients.size());
        }
        return added;
    }

    @Override
    public boolean addAnimal(String name, String ownerName, Species species, String race, String state) throws RemoteException {
        Animal patient = new AnimalImpl(name, ownerName, species, race, state);
        boolean added = patients.add(patient);
        if(added && (patients.size() == THRESHOLD_1 || patients.size() == THRESHOLD_2 || patients.size() == THRESHOLD_3)) {
            notifyObserversOnPatientCountEqualsThreshold(patients.size());
        }
        return added;
    }

    @Override
    public ArrayList<Animal> getAnimals() throws RemoteException {
        return patients;
    }

    @Override
    public Animal getAnimalByName(String name) throws RemoteException {
        for(Animal patient: patients) {
            if(patient.getName().equals(name)) {
                return patient;
            }
        }
        return null;
    }

    @Override
    public void registerObserver(CabinetObserver observer) throws RemoteException {
        observers.add(observer);
    }

    @Override
    public void unregisterObserver(CabinetObserver observer) throws RemoteException {
        observers.remove(observer);
    }

    private void notifyObserversOnPatientCountEqualsThreshold(int threshold) throws RemoteException {
        for (CabinetObserver observer: observers) {
            observer.onPatientCountEqualsThreshold(threshold);
        }
    }
}
