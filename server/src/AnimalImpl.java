import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class AnimalImpl extends UnicastRemoteObject implements Animal {

    String name;
    String ownerName;
    Species species;
    String race;
    FollowUpFile followUpFile;

    public AnimalImpl(String name, String ownerName, String speciesName, int speciesAverageLife, String race, String state) throws RemoteException {
        this.name = name;
        this.ownerName = ownerName;
        this.species = new Species(speciesName, speciesAverageLife);
        this.race = race;
        this.followUpFile = new FollowUpFileImpl(state);
    }

    public AnimalImpl(String name, String ownerName, Species species, String race, String state) throws RemoteException {
        this.name = name;
        this.ownerName = ownerName;
        this.species = species;
        this.race = race;
        this.followUpFile = new FollowUpFileImpl(state);
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }

    @Override
    public String getOwnerName() throws RemoteException {
        return ownerName;
    }

    @Override
    public Species getSpecies() throws RemoteException {
        return species;
    }

    @Override
    public String getRace() throws RemoteException {
        return race;
    }

    @Override
    public FollowUpFile getFollowUpFile() throws RemoteException {
        return followUpFile;
    }
}
