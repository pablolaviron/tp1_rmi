import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class FollowUpFileImpl extends UnicastRemoteObject implements FollowUpFile {

    String state;

    public FollowUpFileImpl(String state) throws RemoteException {
        this.state = state + "\n";
    }

    @Override
    public String read() throws RemoteException {
        return state;
    }

    @Override
    public void add(String newState) throws RemoteException {
        state += newState + "\n";
    }
}
