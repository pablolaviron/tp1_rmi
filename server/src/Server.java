import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public Server() {

    }

    public void setUp() throws RemoteException, AlreadyBoundException {
        System.setProperty("java.security.policy", "server/policies/security.policy");
        System.setSecurityManager(new SecurityManager());
        String currentWorkingDir = System.getProperty("user.dir").replace(" ", "%20");
        System.setProperty("java.rmi.server.codebase", "file://" + currentWorkingDir + "/out/production/client/");
        Cabinet cabinet = new CabinetImpl();
        cabinet.addAnimal("Meow", "Pablo", "Cat", 17, "Maine Coon", "13/01 : OK");
        cabinet.addAnimal("Woof", "Théo", new Species("Dog", 12), "Akita Chow", "07/02 : OK");
        Registry registry = LocateRegistry.createRegistry(1099);
        if(registry == null) {
            System.err.println("Registry not found");
        } else {
            registry.bind("Animal Care", cabinet);
            System.err.println("Server is ready");
        }
    }
}
